a. # Ejemplo de lambda function
suma = lambda x, y: x + y
resultado = suma(3, 5)
print(resultado)  # Imprimirá 8

b. # Definir una lookup table de países y sus códigos de país
country_codes = {
    "Estados Unidos": "USA",
    "Canadá": "CAN",
    "México": "MEX",
    "España": "ESP",
    "Francia": "FRA"
}

# Utilizar la lookup table para obtener el código de un país
pais = "México"
if pais in country_codes:
    codigo = country_codes[pais]
    print(f"El código de {pais} es {codigo}")
else:
    print(f"No se encontró el código para {pais}")

c. Función de fibonacci
def fibonacci(n):
    if n <= 0:
        return 0
    elif n == 1:
        return 1
    else:
        return fibonacci(n-1) + fibonacci(n-2)

# Ejemplo de uso
n = 6
resultado = fibonacci(n)
print(f"El {n}-ésimo valor de la secuencia de Fibonacci es: {resultado}")
d. Ejemplo de quick sort 
def quick_sort(arr):
    if len(arr) <= 1:
        return arr
    else:
        pivot = arr[0]
        lesser = [x for x in arr[1:] if x <= pivot]
        greater = [x for x in arr[1:] if x > pivot]
        return quick_sort(lesser) + [pivot] + quick_sort(greater)

# Ejemplo de uso
arr = [7, 2, 1, 6, 8, 5, 3, 4]
sorted_arr = quick_sort(arr)
print("Arreglo ordenado:", sorted_arr)
