# Logistics - Django

## 25-06-2023 - 1.0.0

    - Added the `requirements.txt` files for any environment
    - Added the `Logistics` app
    - Added the api for the `Logistics` app
    - Organized the project structure
    - Pagination added to the api's

## 24-06-2023 - 0.1.0

    - Django project created
    - Logistics app created
    - Documentation for use the app is updated

## 24-06-2023 - 0.0.1

    - First version of the project
    - Created by: @alberto129
    - Gitignore file created
