from logistics.models import Carrier, Client, Package
from logistics.models.packages import DELIVERY_STATUS
from rest_framework import serializers


class PackageSerializer(serializers.Serializer):
    weight = serializers.CharField(max_length=100)
    origin = serializers.CharField(max_length=100)
    destination = serializers.CharField(max_length=100)
    status = serializers.CharField(max_length=100)
    client = serializers.SlugRelatedField(
        many=False,
        slug_field='id',
        queryset=Client.objects.all().prefetch_related('packages'),
        required=False
    )
    carriers = serializers.SlugRelatedField(
        many=True,
        slug_field='id',
        queryset=Carrier.objects.all().prefetch_related('packages'),
        required=False
    )

    def validate(self, attrs):
        status = attrs.get('status', None)
        if status:
            if status not in [x[0] for x in DELIVERY_STATUS]:
                raise serializers.ValidationError({"status": "Invalid status, must be one of: {}".format(
                    [x[0] for x in DELIVERY_STATUS])})
        return attrs

    def create(self, validated_data):
        validated_data.pop('carriers', None)
        return Package.objects.create(**validated_data)

    def update(self, instance, validated_data):
        instance.weight = validated_data.get('weight', instance.weight)
        instance.origin = validated_data.get('origin', instance.origin)
        instance.destination = validated_data.get('destination', instance.destination)
        instance.status = validated_data.get('status', instance.status)
        instance.client = validated_data.get('client', instance.client)
        instance.save()
        return instance

    class Meta:
        model = Package
        fields = '__all__'
