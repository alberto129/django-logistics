from logistics.models import Client
from rest_framework import serializers


class ClientSerializer(serializers.Serializer):
    first_name = serializers.CharField(max_length=100, required=True)
    last_name = serializers.CharField(max_length=100, required=True)
    email = serializers.CharField(max_length=100, required=True)
    address = serializers.CharField(max_length=100, required=False)
    phone_number = serializers.CharField(max_length=100, required=True)

    def create(self, validated_data):
        return Client.objects.create(**validated_data)

    class Meta:
        model = Client
        fields = '__all__'