from logistics.models import Carrier
from logistics.models.carriers import VEHICLE_TYPE
from rest_framework import serializers


class CarrierSerializer(serializers.Serializer):
    name = serializers.CharField(max_length=100, required=True)
    vehicle_type = serializers.CharField(max_length=100, required=True)
    phone_number = serializers.CharField(max_length=100, required=True)
    packages = serializers.SlugRelatedField(
        many=True,
        slug_field='id',
        queryset=Carrier.objects.all().prefetch_related('packages'),
        required=False
    )

    def validate(self, attrs):
        vehicle_type = attrs.get('vehicle_type', None)
        if vehicle_type:
            if vehicle_type not in [x[0] for x in VEHICLE_TYPE]:
                raise serializers.ValidationError({"vehicle_type": "Invalid vehicle type"})
        return attrs
    def create(self, validated_data):
        return Carrier.objects.create(**validated_data)

    class Meta:
        model = Carrier
        fields = '__all__'