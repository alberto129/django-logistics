from django.urls import include, path
from logistics import views
from rest_framework.routers import DefaultRouter

router = DefaultRouter()
router.register(r'packages', views.PackageViewSet, basename='packages')
router.register(r'clients', views.ClientViewSet, basename='clients')
router.register(r'carriers', views.CarrierViewSet, basename='carriers')

urlpatterns = [
    path('', include(router.urls))
]
