from django.db import models

from .clients import Client

DELIVERY_STATUS = (
    ('pending', 'Pending'),
    ('delivered', 'Delivered'),
    ('cancelled', 'Cancelled'),
)

class Package(models.Model):
    client = models.ForeignKey(Client,
                               on_delete=models.CASCADE,
                               related_name='packages',
                               null=True)
    weight = models.CharField(max_length=100)
    origin = models.CharField(max_length=100)
    destination = models.CharField(max_length=100)
    status = models.CharField(max_length=100,
                              choices=DELIVERY_STATUS,
                              default=DELIVERY_STATUS[0][0])

    def __str__(self):
        return self.destination