from django.db import models

from .packages import Package

VEHICLE_TYPE = (
    ('truck', 'Truck'),
    ('ship', 'Ship'),
    ('plane', 'Plane'),
)

class Carrier(models.Model):
    name = models.CharField(max_length=100)
    vehicle_type = models.CharField(max_length=100, choices=VEHICLE_TYPE)
    phone_number = models.CharField(max_length=100)
    packages = models.ManyToManyField(Package, related_name='carriers')

    def __str__(self):
        return self.name