from logistics.models.carriers import Carrier
from logistics.serializers.carriers import CarrierSerializer
from rest_framework import status, viewsets
from rest_framework.response import Response


class CarrierViewSet(viewsets.ModelViewSet):
    queryset = Carrier.objects.all().prefetch_related('packages')
    serializer_class = CarrierSerializer
    lookup_field = 'id'
    ordering = ('id',)

    def list(self, request):
        queryset = self.filter_queryset(self.get_queryset())
        page = self.paginate_queryset(queryset)
        if page is not None:
            serializer = CarrierSerializer(page, many=True)
            return self.get_paginated_response(serializer.data)
        serializer = CarrierSerializer(queryset, many=True)
        return Response(serializer.data,
                        status=status.HTTP_200_OK)

    def create(self, request):
        serializer = CarrierSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data,
                            status=status.HTTP_201_CREATED)
        return Response(serializer.errors,
                        status=status.HTTP_400_BAD_REQUEST)

    def update(self, request, pk=None):
        serializer = CarrierSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data,
                            status=status.HTTP_200_OK)
        return Response(serializer.errors,
                        status=status.HTTP_400_BAD_REQUEST)

    def destroy(self, request, pk=None):
        package = Carrier.objects.get(id=pk)
        package.delete()
        return Response(status=status.HTTP_204_NO_CONTENT)