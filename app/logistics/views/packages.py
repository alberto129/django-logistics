from django.shortcuts import get_object_or_404
from django_filters.rest_framework import DjangoFilterBackend
from logistics.models import Carrier, Client, Package
from logistics.models.packages import DELIVERY_STATUS
from logistics.serializers.packages import PackageSerializer
from rest_framework import status, viewsets
from rest_framework.decorators import action
from rest_framework.generics import ListAPIView
from rest_framework.response import Response


class PackageViewSet(viewsets.ModelViewSet, ListAPIView):
    """ Package API
    This viewset automatically provides `list`, `create`, `retrieve`,
    `update` and `destroy` actions.
    You can also use the `filter` action to filter packages by client, carrier or status.
    """
    queryset = Package.objects.all().prefetch_related('client', 'carriers')
    serializer_class = PackageSerializer
    lookup_field = 'id'
    filter_backends = [DjangoFilterBackend]
    filterset_fields = ('client', 'carriers__id', 'status',)


    def list(self, request):
        queryset = self.filter_queryset(self.get_queryset())
        page = self.paginate_queryset(queryset)
        if page is not None:
            serializer = self.get_serializer(page, many=True)
            return self.get_paginated_response(serializer.data)
        serializer = self.get_serializer(queryset, many=True)
        return Response(serializer.data,
                        status=status.HTTP_200_OK)

    def create(self, request):
        serializer = PackageSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data,
                            status=status.HTTP_201_CREATED)
        return Response(serializer.errors,
                        status=status.HTTP_400_BAD_REQUEST)

    def update(self, request, id=None):
        package = self.get_object()
        serializer = PackageSerializer(package, data=request.data, partial=True)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data,
                            status=status.HTTP_200_OK)
        return Response(serializer.errors,
                        status=status.HTTP_400_BAD_REQUEST)

    def destroy(self, request, id=None):
        package = Package.objects.get(id=id)
        package.delete()
        return Response(status=status.HTTP_204_NO_CONTENT)

    @action(detail=False, methods=['post'])
    def assign_package_client(self, request):
        if not request.data.get('package'):
            return Response({"error": "Package id was not entered"}, status=status.HTTP_400_BAD_REQUEST)
        package = Package.objects.get(id=request.data.get('package'))
        serializer = PackageSerializer(package, data=request.data, partial=True)
        serializer.is_valid(raise_exception=True)
        serializer.save()
        return Response({"success: ": "The package {} was added successfully to the client {}".format(package.id,
                        serializer.validated_data["client"])},
                        status=status.HTTP_200_OK)

    @action(detail=False, methods=['post'])
    def assign_package_carrier(self, request):
        package_id = request.data.get('package_id', None)
        if not package_id:
            return Response({"error": "Package id was not entered"}, status=status.HTTP_400_BAD_REQUEST)
        package = get_object_or_404(Package, id=package_id)
        carrier_id = request.data.get('carrier_id', None)
        if not carrier_id:
            return Response({"error": "Carrier id was not entered"}, status=status.HTTP_400_BAD_REQUEST)
        carrier = get_object_or_404(Carrier, id=carrier_id)
        # Create the relationship between the package and the carrier
        carrier.packages.add(package)
        return Response({"The package {} was added successfully to the carrier {}".format(package_id, carrier.name)},
                        status=status.HTTP_200_OK)

    @action(detail=False, methods=['patch'])
    def change_package_status(self, request):
        if not request.data.get('package'):
            return Response({"error": "Package id was not entered"}, status=status.HTTP_400_BAD_REQUEST)
        package = Package.objects.get(id=request.data.get('package'))
        serializer = PackageSerializer(package, data=request.data, partial=True)
        serializer.is_valid(raise_exception=True)
        serializer.save()
        return Response({"The package with destination {}, status was changed to {}".format(package,
                        serializer.validated_data["status"])},
                        status=status.HTTP_200_OK)