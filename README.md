# Logistics Backend Django
- This project is a backend for a logistics company, where you can create, update, delete and list the orders, and also you can create, update, delete and list the products.


## Getting started

1. Clone the repository
2. To run the project, you need to use the following commands:

- 'python3 -m venv .env-logistics' to create a virtual environment
- '. .env-logistics/bin/activate' to activate the virtual environment
- 'pip install -U -r ./build_files/requirements/base.txt' to install the dependencies
- 'pip install -U -r ./build_files/requirements/devevelop.txt' to install the dev dependencies

- then, you can run the project with the command 'python manage.py runserver'

## Documentation
- With the url 'https://api.postman.com/collections/19257258-73e9e495-8c9c-41ba-ad55-8787280dc4a9?access_key=PMAT-01H3SGMF0F65K9AMB0RMZP8EYEJ' you can import the postman collection to test the api's, but you need to create the local environment and variables to your own values.
